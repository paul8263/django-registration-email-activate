from django.template.response import TemplateResponse
from django.shortcuts import resolve_url
from django.core.urlresolvers import reverse
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.tokens import default_token_generator
from django.utils.translation import ugettext_lazy as _
import warnings
from django.utils.deprecation import RemovedInDjango110Warning
from django.http import HttpResponseRedirect
from django.contrib.auth import get_user_model
from django.utils.http import urlsafe_base64_decode
from django.utils.encoding import force_text
from .forms import RegistrationForm

# Create your views here.
@never_cache
@csrf_protect
def register(request, is_admin_site=False,
             template_name='registration/registration_form.html',
             email_template_name='registration/activation_email.html',
             subject_template_name='registration/activation_email_subject.txt',
             registration_form=RegistrationForm,
             token_generator=default_token_generator,
             post_registration_redirect=None,
             from_email=None,
             current_app=None,
             extra_context=None,
             html_email_template_name=None):

    if post_registration_redirect is None:
        post_registration_redirect = reverse('register_done')
    else:
        post_registration_redirect = resolve_url(post_registration_redirect)
    if request.method == "POST":
        form = registration_form(request.POST)
        if form.is_valid():
            opts = {
                'use_https': request.is_secure(),
                'token_generator': token_generator,
                'from_email': from_email,
                'email_template_name': email_template_name,
                'subject_template_name': subject_template_name,
                'request': request,
                'html_email_template_name': html_email_template_name,
            }
            if is_admin_site:
                warnings.warn(
                    "The is_admin_site argument to "
                    "django.contrib.auth.views.password_reset() is deprecated "
                    "and will be removed in Django 1.10.",
                    RemovedInDjango110Warning, 3
                )
                opts = dict(opts, domain_override=request.get_host())
            form.save(**opts)
            return HttpResponseRedirect(post_registration_redirect)
    else:
        form = registration_form()

    context = {
        'form': form,
        'title': _('Password reset'),
    }

    if extra_context is not None:
        context.update(extra_context)

    if current_app is not None:
        request.current_app = current_app

    return TemplateResponse(request, template_name, context)


def register_done(request, template_name='registration/registration_done.html',
                  current_app=None, extra_context=None):
    context = {
        'title': _('Password reset sent'),
    }
    if extra_context is not None:
        context.update(extra_context)

    if current_app is not None:
        request.current_app = current_app

    return TemplateResponse(request, template_name, context)


def user_activate(request, uidb64=None, token=None,
                  template_name='registration/activation_done.html',
                  token_generator=default_token_generator,
                  current_app=None, extra_context=None):
    """
    View that checks the hash in a activation link and redirect to
    activation done page
    """
    UserModel = get_user_model()
    assert uidb64 is not None and token is not None  # checked by URLconf

    try:
        # urlsafe_base64_decode() decodes to bytestring on Python 3
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = UserModel._default_manager.get(pk=uid)
    except (TypeError, ValueError, OverflowError, UserModel.DoesNotExist):
        user = None

    if user is not None and token_generator.check_token(user, token):
        validlink = True
        title = _('User activation success')
        user.is_active = True
        user.save()

    else:
        validlink = False
        title = _('User activation failure')
    context = {
        'title': title,
        'validlink': validlink,
    }
    if extra_context is not None:
        context.update(extra_context)

    if current_app is not None:
        request.current_app = current_app

    return TemplateResponse(request, template_name, context)

