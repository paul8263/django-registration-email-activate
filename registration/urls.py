__author__ = 'paulzhang'
from django.conf.urls import include, url
from . import views

urlpatterns = [
    url(r'^register/$', views.register, name='register'),
    url(r'^register/done/$', views.register_done, name='register_done'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', views.user_activate, name='activate'),
]
