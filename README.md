# Django Registration Email Activate

### Project Description

> The idea of this project is inspired by Django provided user password reset.

This project enables two-step user registration:

1. User provides basic information. After submitting, the is_active state is set to False, awaiting for user activate.
2. After registration request submitted, an activation request email will be send to the provided email address.
3. The user account is activated by clicking the URL shown in this email. If success, a success page will be redirected to.
4. If the activation link is invalid (a tampered URL or unused for more than 3 days by default, determined by **PASSWORD_RESET_TIMEOUT_DAYS** in settings.py), an activation failure page will show.

### Project Dependencies:
* Django widget tweaks
