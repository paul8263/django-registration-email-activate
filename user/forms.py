__author__ = 'paulzhang'
from . import models
from registration import forms


class UserProfileForm(forms.RegistrationForm):
    '''
    How to build customised registration form:
    1. Create customised user model
    2. Create form class. It should extend Registration.forms.RegistrationForm
    3. Create the Meta class. It should extend the Registration.forms.RegistrationForm.Meta
    4. Make should the field 'date_joined' is excluded
    '''
    class Meta(forms.RegistrationForm.Meta):
        model = models.UserProfile
        exclude = ['date_joined']
