from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class UserProfile(User):
    '''

    Customised user model should extend django.contrib.auth.models.User class

    And add extra fields here

    '''
    # Extra fields
    nickname = models.CharField(max_length=100, blank=True)
