__author__ = 'paulzhang'

from django.conf.urls import include, url
from registration import views as registration_view
from . import forms

urlpatterns = [
    url(r'^register/$', registration_view.register,
        {'registration_form': forms.UserProfileForm, 'template_name': 'user/registration_form.html'}, name='register'),
    url(r'^register/done/$', registration_view.register_done, name='register_done'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        registration_view.user_activate, name='activate'),
]
